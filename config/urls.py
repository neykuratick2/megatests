from django.contrib import admin
from django.http import HttpRequest
from django.shortcuts import render
from django.urls import path
from django.urls import include
from munch import DefaultMunch


def home(request: HttpRequest):
    buttons = [
        DefaultMunch.fromDict({"href": "quiz-list", "title": "Посмотреть тесты"}),
        DefaultMunch.fromDict({"href": "user-login", "title": "Войти"}),
        DefaultMunch.fromDict({"href": "user-create", "title": "Зарегистрироваться"}),
    ]

    if request.user.is_authenticated:
        buttons = [
            DefaultMunch.fromDict({"href": "quiz-create", "title": "Создать тест"}),
            DefaultMunch.fromDict({"href": "quiz-list", "title": "Посмотреть тесты"}),
            DefaultMunch.fromDict({"href": "user-logout", "title": "Выйти"}),
        ]

    context = {"buttons": buttons}
    return render(request=request, template_name="base.html", context=context)


urlpatterns = [
    path("", home, name="home"),
    path("admin/", admin.site.urls),
    path("", include("app.quiz.urls")),
    path("", include("app.users.urls")),
]
