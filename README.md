# Local

- `cp data/docker-compose-local.yml docker-compose.yml`
- ` cp data/.env.local.example .env.local `
- Настраиваем все параметры
    
    **Важно!!** значение `DB_HOST` должно быть == названию докер контейнера с постгресом
- ` docker-compose up --build `

# Production

- Копировать файлик с конфигурацией докера не надо, это сделает раннер
- Настроить раннер в репозитории гатлаба
- Настроить файлик `.env.prod`, как в Local 
- Создать следующие параметры окружения для репозитория:
- - `MASTER_HOST` - VPS IP. Тип: Variable
- - `MASTER_SSH_KEY` - VPS openssh private key. Тип: File
- - `MASTER_SSH_USER` - Имя юзера на VPS (например, root). Тип: Variable
- - `ENV_FILE_PROD` - содержание файлика `.env.prod`. Тип: File
- Выполнить инструкцию для получения SSL сертификата https://pentacent.medium.com/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71