import ast
import json

from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import UpdateView

from app.quiz.forms import AnswersForm
from app.quiz.forms import AnswersFormCreateSet
from app.quiz.forms import AnswersFormReadSet
from app.quiz.forms import QuestionsFormCreateSet
from app.quiz.forms import QuizForm
from app.quiz.models import AnswerModel
from app.quiz.models import QuestionModel
from app.quiz.models import QuizModel
from app.quiz.response_tracker import ResponseTracker
from app.users.models import User


def test(request):
    print(request.POST)


class QuizList(ListView):
    model = QuizModel
    template_name = "quiz/get_all.html"
    allow_empty = False
    queryset = QuizModel.objects.all()


class QuizDetailed(DetailView):
    model = QuizModel
    template_name = "quiz/get_one.html"
    allow_empty = False
    slug_url_kwarg = "custom_uuid"

    def post(self, *args, **kwargs):
        kwargs = dict(args[0].POST)
        scores = 0
        for index, option in enumerate(kwargs.get('options')):
            option = int(option)
            scored_raw = kwargs.get('scores')[index]
            right_answer_raw = kwargs.get('right_answer')[index].replace('true', 'True').replace('false', 'False')
            scored = list(eval(scored_raw))[option]
            right_answer = list(eval(right_answer_raw))[option]
            scores += scored
            print(option, scored, right_answer)

        kikorik = None
        if scores > 100000:
            kikorik = "не смешарик"
        if scores > 10000 and scores < 100000:
            kikorik = "Бараш"
        elif scores > 1000 and scores < 10000:
            kikorik = "Пин"
        elif scores > 600 and scores < 1000:
            kikorik = "Копатыч"
        elif scores > 10 and scores < 600:
            kikorik = "Крош"

        return render(context={'scores': scores, "kikorik": kikorik}, template_name='quiz/scores.html', request=args[0])

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(QuizDetailed, self).get_context_data(**kwargs)
        quiz = context.get("object")

        questions = QuestionModel.objects.filter(quiz=quiz)

        pair = []
        for question in questions:
            answers = AnswerModel.objects.filter(question=question).all()
            choices = [(index, item.text) for index, item in enumerate(list(answers))]
            scores = [item.scores for item in list(answers)]
            right_answer = [item.right_answer for item in list(answers)]
            pair.append((question, AnswersForm(choices=choices, scores=scores, right_answer=right_answer), [answer.right_answer for answer in answers], [answer.scores for answer in answers]))

        context["quiz"] = quiz
        context["uuid"] = ResponseTracker().initialize_response()
        context["questions"] = questions
        context["pair"] = pair

        return context

    def get_object(self, queryset=None):
        if pk := self.kwargs.get("pk"):
            return QuizModel.objects.get(id=pk)

        if custom_uuid := self.kwargs["custom_uuid"]:
            return QuizModel.objects.get(custom_uuid=custom_uuid)


class QuizCreate(CreateView):
    template_name = "quiz/create.html"
    success_url = reverse_lazy("quiz-list")

    def get(self, request, *args, **kwargs):
        questions_formset = QuestionsFormCreateSet(
            queryset=QuestionModel.objects.none()
        )
        answers_formset = AnswersFormCreateSet(queryset=AnswerModel.objects.none())

        return self.render_to_response(
            {
                "quiz": QuizForm(),
                "questions_formset": questions_formset,
                "answers_formset": answers_formset,
            }
        )

    def post(self, request, *args, **kwargs):
        if not request.user:
            raise NotImplemented("User must be logged in")

        quiz_form = QuizForm(request.POST)
        request_dict = dict(request.POST.lists())

        quiz = quiz_form.save(commit=False)
        quiz.owner = User.objects.get(id=request.user.id)
        quiz.save()

        total_forms = int(request_dict.get("form-TOTAL_FORMS")[0])

        for i in range(total_forms):
            title = request_dict.get(f"form-{i}-title")[0]
            description = request_dict.get(f"form-{i}-description")[0]

            question = QuestionModel(
                title=title,
                description=description,
                quiz=QuizModel.objects.get(id=quiz.id),
            )
            question.save()

            text = request_dict.get(f"form-{i}-text")
            scores = request_dict.get(f"form-{i}-scores")
            right_answer = request_dict.get(f"form-{i}-right_answer")

            [
                AnswerModel(
                    text=i[0],
                    scores=i[1],
                    right_answer=i[2] is True,
                    question=QuestionModel.objects.get(id=question.id),
                ).save()
                for i in zip(text, scores, right_answer)
            ]

        return redirect(reverse_lazy("quiz-list"))


class QuizDelete(DeleteView):
    model = QuizModel
    template_name = "quiz/delete.html"
    success_url = reverse_lazy("quiz-list")


class QuizUpdate(UpdateView):
    model = QuizModel
    fields = "__all__"
    template_name = "quiz/update.html"
