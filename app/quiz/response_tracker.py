from enum import Enum

from app.utils import Singleton
from uuid import uuid4


@Singleton
class ResponseTracker(object):
    def __init__(self):
        self.responses = {}

    def initialize_response(self) -> str:
        uuid = str(uuid4())
        self.responses[uuid] = []
        return uuid

    def add_record_to_response(self, uuid: str, scores, right_answer):
        self.responses[uuid].append({"scores": scores, "right_answer": right_answer})

    def flush(self, uuid: str):
        self.responses.pop(uuid)

    def get_response_status(self, uuid: str) -> dict:
        return self.responses.get(uuid)
