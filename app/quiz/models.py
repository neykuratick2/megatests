from django.db.models import BooleanField
from django.db.models import CASCADE
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import ForeignKey
from django.db.models import IntegerField
from django.db.models import Model
from django.db.models import TextField

from app.users.models import User


class QuizModel(Model):
    title = CharField(max_length=100)
    description = TextField(null=True, blank=True)
    custom_uuid = CharField(max_length=10, unique=True, null=True, blank=True)
    updated = DateTimeField(auto_now=True)
    created = DateTimeField(auto_now_add=True)

    owner = ForeignKey(User, on_delete=CASCADE, null=True)

    def __str__(self):
        return f"Заголовок: {self.title}\n" f"Описание: {self.description or 'Нет'}\n\n"

    def get_absolute_url(self):
        if self.custom_uuid:
            return f"/quiz/{self.custom_uuid}"

        return f"/quiz/id/{self.id}"


class QuestionModel(Model):
    title = CharField(max_length=300)
    description = TextField(null=True, blank=True)

    updated = DateTimeField(auto_now=True)
    created = DateTimeField(auto_now_add=True)

    quiz = ForeignKey(QuizModel, on_delete=CASCADE)

    def __str__(self):
        return f"Заголовок: {self.title}\n" f"Описание: {self.description or 'Нет'}\n\n"


class AnswerModel(Model):
    text = CharField(max_length=300)
    scores = IntegerField(blank=True, default=0)
    right_answer = BooleanField(blank=True, default=False)

    updated = DateTimeField(auto_now=True)
    created = DateTimeField(auto_now_add=True)

    question = ForeignKey(QuestionModel, on_delete=CASCADE)

    def __str__(self):
        return f"Заголовок: {self.text}\n" f"Очки: {self.scores or 'Нет'}\n\n"
