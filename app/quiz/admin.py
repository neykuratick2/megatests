from django.contrib import admin

from app.quiz.models import AnswerModel
from app.quiz.models import QuestionModel
from app.quiz.models import QuizModel


@admin.register(QuizModel)
class UserAdmin(admin.ModelAdmin):
    list_display = ("title", "description")


@admin.register(QuestionModel)
class UserAdmin(admin.ModelAdmin):
    list_display = ("title", "description")


@admin.register(AnswerModel)
class UserAdmin(admin.ModelAdmin):
    list_display = ("text", "scores")
