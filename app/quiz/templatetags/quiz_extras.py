from django import template

from app.quiz.forms import AnswersForm
from app.quiz.models import AnswerModel
from app.quiz.models import QuestionModel
from app.quiz.models import QuizModel

register = template.Library()


@register.filter
def get_href(_, quiz_model: QuizModel):
    if name := quiz_model.custom_uuid:
        return f"/quiz/{name}"
    else:
        return f"/quiz/id/{str(quiz_model.id)}"


@register.filter
def get_question_answers(_, question: QuestionModel):
    answers = list(AnswerModel.objects.filter(question=question).all())

    choices = [(index, item.text) for index, item in enumerate(answers)]
    form = AnswersForm(choices=choices)
    return form


@register.filter
def add_answer(_, question: QuestionModel):
    answers = list(AnswerModel.objects.filter(question=question).all())

    choices = [(index, item.text) for index, item in enumerate(answers)]
    form = AnswersForm(choices=choices)
    return form


@register.filter
def model_to_dict(_, obj):
    return {
        x: obj.__dict__[x]
        for x in obj.__dict__
        if x in {y.column for y in obj._meta.fields}
    }
