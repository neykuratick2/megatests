from django.urls import path

from app.quiz.views import QuizCreate
from app.quiz.views import QuizDelete
from app.quiz.views import QuizDetailed
from app.quiz.views import QuizList
from app.quiz.views import QuizUpdate
from app.quiz.views import test

urlpatterns = [
    path("quizzes/create", QuizCreate.as_view(), name="quiz-create"),
    path("quizzes/submit-answer", test, name="quiz-submit-answer"),
    path("quizzes/update/<int:pk>/", QuizUpdate.as_view(), name="quiz-update"),
    path("quizzes/delete/<int:pk>/", QuizDelete.as_view(), name="quiz-delete"),
    path("quizzes/", QuizList.as_view(), name="quiz-list"),
    path("quiz/id/<int:pk>/", QuizDetailed.as_view(), name="quiz-detail"),
    path("quiz/<slug:custom_uuid>/", QuizDetailed.as_view(), name="quiz-detail"),
]
