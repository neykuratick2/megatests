from django import forms
from django.forms import modelformset_factory

from app.quiz.models import AnswerModel
from app.quiz.models import QuestionModel
from app.quiz.models import QuizModel
from django.forms import formset_factory


class QuizForm(forms.ModelForm):
    class Meta:
        model = QuizModel
        fields = ("title", "description", "custom_uuid")


class QuestionFormCreate(forms.ModelForm):
    class Meta:
        model = QuestionModel
        fields = ("title", "description")

        widgets = {
            "title": forms.TextInput(attrs={"class": "form-control"}),
            "description": forms.TextInput(attrs={"class": "form-control"}),
        }


class AnswerFormCreate(forms.ModelForm):
    class Meta:
        model = AnswerModel
        fields = ("text", "scores", "right_answer")

        widgets = {
            "text": forms.TextInput(attrs={"class": "form-control"}),
            "scores": forms.NumberInput(attrs={"class": "form-control"}),
            "right_answer": forms.NullBooleanSelect(),
        }


class AnswersForm(forms.Form):
    def __init__(self, choices, scores, right_answer, *args, **kwargs):
        super(AnswersForm, self).__init__(*args, **kwargs)
        self.fields["options"] = forms.ChoiceField(
            widget=forms.Select, choices=choices
        )
        self.fields["scores"] = forms.JSONField(widget=forms.HiddenInput, initial=scores)
        self.fields["right_answer"] = forms.JSONField(widget=forms.HiddenInput, initial=right_answer)


QuestionsFormCreateSet = modelformset_factory(
    model=QuestionModel,
    form=QuestionFormCreate,
    fields=("title", "description"),
    extra=1,
)


AnswersFormCreateSet = modelformset_factory(
    model=AnswerModel,
    form=AnswerFormCreate,
    fields=("text", "scores", "right_answer"),
    extra=1,
)


AnswersFormReadSet = formset_factory(form=AnswersForm)
