from django import forms
from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    GENDERS = (
        ("mf", "Мужчина ассоциирующий себя с женщиной"),
        ("fm", "Женщина ассоциирующая себя с мужчиной"),
        ("tm", "Трансмужчина"),
        ("tf", "Трансженщина"),
        ("sue", "Суперпозиция - и не женщина и не мужчина"),
        ("sui", "Суперпозиция - и женщина и мужчина одновременно"),
        ("nb", "Не бинарный"),
        ("ah", "Атакующий вертолётик"),
        ("cf", "Кошка-девочка"),
        ("cm", "Кошка-мальчик"),
        ("cd", "Котопёс"),
        ("un", "Я не знаю свой гендер"),
        ("pri", "Предпочитаю не говорить"),
    )

    gender = models.CharField(max_length=255, choices=GENDERS, verbose_name="Гендер")
    birth_date = models.DateTimeField(
        verbose_name="Дата рождения", blank=True, null=True
    )
