from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render
from app.users.models import User


class UserRegisterForm(UserCreationForm):
    birth_date = forms.DateField(
        label="Дата рождения", widget=forms.TextInput(attrs={"type": "date"})
    )

    first_name = forms.CharField(label="Имя")
    last_name = forms.CharField(label="Фамилия")
    email = forms.CharField(label="Эл. почта")
    username = forms.CharField(label="Имя пользователя")

    password1 = forms.CharField(
        label="Пароль",
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label="Подтверждение пароля",
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        strip=False,
        help_text="Повторите введённый ранее пароль",
    )

    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
            "birth_date",
            "gender",
            "email",
            "username",
        ]
