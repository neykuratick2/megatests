from django.urls import path
from app.users.views import LoginUser
from app.users.views import LogoutUser
from app.users.views import UserCreate

urlpatterns = [
    path("users/create", UserCreate.as_view(), name="user-create"),
    path("users/login", LoginUser.as_view(), name="user-login"),
    path("users/logout", LogoutUser.as_view(), name="user-logout"),
]
